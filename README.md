# Coding Standards and Naming Conventions

Coding Standards and Naming Conventions for Sapawarga apps development

## Coding Standards

### Konfigurasi disimpan di environment variable, bukan hardcoded

Contoh kurang baik:

```js
export function hoaxTypes() {
  return request({
    url: `http://52.74.74.33:3000/v1/hoax-types`,
    method: 'get'
  })
}

```

Contoh lebih baik:

```js
export function hoaxTypes() {
  return request({
    url: process.env.HOAX_SERVICE_URL,
    method: 'get'
  })
}

```

### Variabel konstan didefinisikan sebagai objek dan menggunakan huruf kapital

Contoh:

```js
// definition
const SurveyStatus = Object.freeze({
    DRAFT: 0,
    ACTIVE: 10
});

// usage
SurveyStatus.DRAFT
```

### Teks label tidak di hardcoded, melainkan didefinisikan pada kamus sesuai bahasa

Contoh kurang baik:

```js
<div class="card-panel-text">
  RW
</div>

```

Contoh lebih baik:

```js
// definition
export default {
  route: {
    ...
    'widget-rw': 'RW',
    ...
  }
}

// usage
<div class="card-panel-text">
  {{ $t('label.widget-rw') }}
</div>
```

## Naming Conventions

### Mengungkapkan intensi tanpa perlu penjelasan lebih lanjut

Contoh kurang baik:

```js
const dateStart = moment(start_date).startOf('day')
const isSameDate = dateStart.isSame(new Date(), 'day')

if ((status === 0) && (isSameDate)) {
  this.btnDisableDate = false
}
```

Contoh lebih baik:

```js
const SurveyStatus = Object.freeze({
    DRAFT: 0,
    ACTIVE: 10
});
const isStartedToday = dateStart.isSame(new Date(), 'day')

// jika status sama dengan draft dan tanggal mulai sama dengan hari ini
if ((status === SurveyStatus.DRAFT && isStartedToday)) {
  this.btnDisableDate = false
}
```

Perhatikan bahwa:

* Dengan mendefinisikan nilai konstan sebagai variabel seperti `status === SurveyStatus.DRAFT` lebih mudah dipahami ketimbang `status === 0`.
* Penamaan variabel `isStartedToday` lebih jelas intensinya ketimbang `isSameDate`.

### Variabel/fungsi Boolean menggunakan ekspresi pertanyaan

Contoh kurang baik:

```js
this.dateButtonDisabled = false

function checkPermission(userId, ruleName){}
```

Contoh lebih baik:

```js
var isDateButtonDisabled = false

function hasPermission(userId, ruleName){}
```
